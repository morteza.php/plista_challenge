<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{

    /**
     * @param Campaign|null $campaign
     * @return array
     */
    public function trim(Campaign $campaign = null)
    {

        if (!$campaign) {
            $campaign = $this;
        }

        return [
            'id' => (int)$campaign->id,
            'title' => (string)$campaign->title,
            'starts_at' => (string)$campaign->starts_at,
            'ends_at' => (string)$campaign->ends_at,
            'created_at' => (string)$campaign->created_at,
            'updated_at' => (string)$campaign->updated_at
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->hasMany(Ad::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advertiser()
    {
        return $this->belongsTo(Advertiser::class);
    }
}
