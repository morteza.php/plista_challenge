<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUsedDevice extends Model
{

    protected $fillable = [
        'type', 'platform', 'user_id', 'saved_at'
    ];


    /**
     * @param UserUsedDevice|null $userUsedDevice
     * @return array
     */
    public function trim(UserUsedDevice $userUsedDevice = null)
    {

        if (!$userUsedDevice) {
            $userUsedDevice = $this;
        }

        return [
            'type' => (string)$userUsedDevice->type,
            'platform' => (string)$userUsedDevice->platform,
            'user_id' => (int)$userUsedDevice->user_id,
            'saved_at' => (string)$userUsedDevice->saved_at
        ];
    }
}
