<?php

namespace App\Http\Controllers\Api\V1;

use App\Ad;
use App\Advertiser;
use App\Campaign;
use App\Helpers\ResponseHelper;
use App\Http\Requests\StoreAdRequest;
use App\Http\Requests\UpdateAdRequest;

class AdController extends Controller
{

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $ad = Ad::findOrFail($id);

        return ResponseHelper::json([
            'tag' => 'OK',
            'results' => [
                'ad' => $ad->trim()
            ]
        ]);
    }


    /**
     * @param $campaignId
     * @return \Illuminate\Http\JsonResponse
     */
    public function adsByCampaignId($campaignId)
    {

        // get $campaign
        $campaign = Campaign::findOrFail($campaignId);


        // get its ads
        $paginatedAds = Ad::where('campaign_id', $campaign->id)->paginate(15);


        // trim ads
        $adds = [];
        foreach ($paginatedAds as $paginatedAd) {
            $adds[] = $paginatedAd->trim();
        }

        // make response
        return ResponseHelper::json([
            'tag' => 'OK',
            'results' => [
                'campaign' => $campaign->trim(),
                'ads' => $adds,
                'paginate' => ResponseHelper::getPaginate($paginatedAds)
            ]
        ]);
    }


    /**
     * @param $advertiserId
     * @return \Illuminate\Http\JsonResponse
     */
    public function adsByAdvertiserId($advertiserId)
    {

        // get $advertiser
        $advertiser = Advertiser::findOrFail($advertiserId);


        // get $campaign
        $campaign = Campaign::where('advertiser_id', $advertiser->id)->first();
        if (!$campaign) {
            return ResponseHelper::json([
                'tag' => 'ZERO_RESULTS',
                'results' => [
                    'advertiser' => $advertiser->trim()
                ]
            ]);
        }


        // get its ads
        $paginatedAds = Ad::where('campaign_id', $campaign->id)->paginate(15);


        // trim ads
        $adds = [];
        foreach ($paginatedAds as $paginatedAd) {
            $adds[] = $paginatedAd->trim();
        }

        // make response
        return ResponseHelper::json([
            'tag' => 'OK',
            'results' => [
                'advertiser' => $advertiser->trim(),
                'campaign' => $campaign->trim(),
                'ads' => $adds,
                'paginate' => ResponseHelper::getPaginate($paginatedAds)
            ]
        ]);
    }


    /**
     * @param StoreAdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAdRequest $request)
    {

        $campaignId = $request->get('campaign_id');

        // check the $campaign
        $campaign = Campaign::findOrFail($campaignId);

        $newAdData = $request->only(['title', 'text', 'image', 'sponsoredBy', 'trackingUrl', 'campaign_id']);

        $ad = new Ad($newAdData);
        $ad->save();


        return ResponseHelper::json([
            'tag' => 'OK',
            'results' => [
                'ad' => $ad->trim(),
                'campaign' => $campaign->trim()
            ]
        ]);
    }


    /**
     * @param UpdateAdRequest $request
     * @param $adId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateAdRequest $request, $adId)
    {

        $campaignId = $request->get('campaign_id');

        // check the $campaign
        $campaign = Campaign::findOrFail($campaignId);

        $ad = Ad::findOrFail($adId);

        $newAdData = $request->only(['title', 'text', 'image', 'sponsoredBy', 'trackingUrl', 'campaign_id']);

        $ad->fill($newAdData);
        $ad->save();


        return ResponseHelper::json([
            'tag' => 'OK',
            'results' => [
                'ad' => $ad->trim(),
                'campaign' => $campaign->trim()
            ]
        ]);
    }
}
