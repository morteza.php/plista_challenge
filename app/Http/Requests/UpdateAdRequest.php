<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'title' => 'string|required|max:255',
            'text' => 'string|required|max:4000',
            'image' => 'url|required|max:255',
            'sponsoredBy' => 'string|required|max:255',
            'trackingUrl' => 'url|required|max:255',
            'campaign_id' => 'int|required|min:1'
        ];
    }

}
