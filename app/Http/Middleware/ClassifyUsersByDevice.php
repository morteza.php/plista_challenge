<?php

namespace App\Http\Middleware;

use App\Helpers\AgentLogger;
use App\UserUsedDevice;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Log;


class ClassifyUsersByDevice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {

            $data = AgentLogger::log($request);

            $data['user_id'] = $request->user() ? $request->user()->id : null;
            $data['saved_at'] = Carbon::now();

            UserUsedDevice::insert($data);

        } catch (\Exception $exception) {
            Log::error($exception);
        }

        return $next($request);
    }
}
