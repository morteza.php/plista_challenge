<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertiser extends Model
{


    public function trim(Advertiser $advertiser = null)
    {

        if (!$advertiser) {
            $advertiser = $this;
        }

        return [
            'id' => (int)$advertiser->id,
            'name' => (string)$advertiser->name,
            'family' => (string)$advertiser->family,
            'created_at' => (string)$advertiser->created_at,
            'updated_at' => (string)$advertiser->updated_at
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
}
