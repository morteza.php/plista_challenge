<?php

namespace App\Helpers;

use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;

class AgentLogger
{

    /**
     * @param Request|null $request
     * @return array
     */
    public static function log(Request $request = null)
    {
        $agent = new Agent();

        if ($request) {
            $agent->setUserAgent($request->userAgent());
            $agent->setHttpHeaders($request->headers->all());
        }


        $type = $agent->isTablet() ? 'tablet' : null;
        if (!$type) {
            $type = $agent->isMobile() ? 'phone' : null;
        }
        if (!$type) {
            $type = $agent->isDesktop() ? 'desktop' : null;
        }
        if (!$type) {
            $type = '__Unknown__';
        }


        $platform = $agent->platform();

        return [
            'type' => $type,
            'platform' => $platform
        ];

    }
}
