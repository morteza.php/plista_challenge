<?php

namespace App\Helpers;


class ResponseHelper
{

    /**
     * @param array $response
     * @return \Illuminate\Http\JsonResponse
     */
    public static function json(array $response)
    {
        if (!isset($response['message'])) {
            $response['message'] = 'no message!';
        }

        if (!isset($response['results'])) {
            $response['results'] = (object)[];
        }


        if (!isset($response['tag'])) {
            $response['tag'] = '__Unknown__';
        }

        return response()->json($response);
    }


    /**
     * @param $items
     * @return array
     */
    public static function getSimplePaginate($items)
    {
        return [
            'count' => (int)$items->count(),
            'currentPage' => (int)$items->currentPage(),
            'firstItem' => (int)$items->firstItem(),
            'hasMorePages' => (int)$items->hasMorePages(),
            'lastItem' => (int)$items->lastItem(),
            'perPage' => (int)$items->perPage()
        ];
    }


    /**
     * @param $items
     * @return array
     */
    public static function getPaginate($items)
    {
        return [
            'count' => (int)$items->count(),
            'currentPage' => (int)$items->currentPage(),
            'firstItem' => (int)$items->firstItem(),
            'hasMorePages' => (int)$items->hasMorePages(),
            'lastItem' => (int)$items->lastItem(),
            'lastPage' => (int)$items->lastPage(),
            'perPage' => (int)$items->perPage(),
            'total' => (int)$items->total()
        ];
    }
}
