<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{

    protected $fillable = [
        'title', 'text', 'image', 'sponsoredBy', 'trackingUrl', 'campaign_id'
    ];

    /**
     * @param Ad|null $ad
     * @return array
     */
    public function trim(Ad $ad = null)
    {

        if (!$ad) {
            $ad = $this;
        }

        return [
            'id' => (int)$ad->id,
            'title' => (string)$ad->title,
            'text' => (string)$ad->text,
            'image' => (string)$ad->image,
            'sponsoredBy' => (string)$ad->sponsoredBy,
            'trackingUrl' => (string)$ad->trackingUrl,
            'created_at' => (string)$ad->created_at,
            'updated_at' => (string)$ad->updated_at
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
}
