<?php


Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api\V1'
], function () {

    // add
    Route::get('ads/{id}', 'AdController@show');
    Route::get('ads/by-campaign/{campaignId}', 'AdController@adsByCampaignId');
    Route::get('ads/by-advertiser/{advertiserId}', 'AdController@adsByAdvertiserId');
    Route::post('ads/store', 'AdController@store');
    Route::put('ads/update/{id}', 'AdController@update');

});
