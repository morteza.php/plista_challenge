<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(\App\Advertiser::class, 2000)->create()->each(function ($advertiser) {
            $campaigns = $advertiser->campaigns()->saveMany(factory(\App\Campaign::class, rand(0, 8))->make());

            foreach ($campaigns as $campaign) {
                $campaign->ads()->saveMany(factory(\App\Ad::class, rand(0, 80))->make());
            }
        });
    }
}
