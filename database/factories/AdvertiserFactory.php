<?php

use Faker\Generator as Faker;

$factory->define(App\Advertiser::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName(),
        'family' => $faker->lastName()
    ];
});
