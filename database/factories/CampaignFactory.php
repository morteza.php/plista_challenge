<?php

use Faker\Generator as Faker;

$factory->define(App\Campaign::class, function (Faker $faker) {

    $startsAt = $faker->dateTimeBetween('-1 years', '+1 years');

    return [
        'title' => $faker->sentence(10, true),
        'starts_at' => $startsAt,
        'ends_at' => $faker->dateTimeBetween($startsAt, '+1 years')
    ];
});
