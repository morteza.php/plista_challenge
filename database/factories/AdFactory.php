<?php

use Faker\Generator as Faker;

$factory->define(App\Ad::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(4, true),
        'text' => $faker->paragraph(),
        'image' => $faker->imageUrl($width = 640, $height = 480),
        'sponsoredBy' => str_random(10),
        'trackingUrl' => $faker->url()
    ];
});
