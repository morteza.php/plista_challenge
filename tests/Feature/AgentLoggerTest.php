<?php

namespace Tests\Feature;

use App\Helpers\AgentLogger;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AgentLoggerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDeviceTypeExample()
    {

        $tabletAgent = 'Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3';
        $phoneAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3';
        $desktopAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2';

        $requests =
            [
                'tablet' => $this->createRequest('GET', null, '/', ['HTTP_USER_AGENT' => $tabletAgent]),
                'phone' => $this->createRequest('GET', null, '/', ['HTTP_USER_AGENT' => $phoneAgent]),
                'desktop' => $this->createRequest('GET', null, '/', ['HTTP_USER_AGENT' => $desktopAgent]),
            ];

        foreach ($requests as $realType => $request) {
            $data = AgentLogger::log($request);

            if ($data['type'] != $realType) {
                $this->assertTrue(false);
            }
        }

        $this->assertTrue(true);
    }


    public function testDevicePlatformExample()
    {

        $macintoshAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2';
        $windowsAgent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0';
        $linuxAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36';

        $requests =
            [
                'OS X' => $this->createRequest('GET', null, '/', ['HTTP_USER_AGENT' => $macintoshAgent]),
                'Windows' => $this->createRequest('GET', null, '/', ['HTTP_USER_AGENT' => $windowsAgent]),
                'Linux' => $this->createRequest('GET', null, '/', ['HTTP_USER_AGENT' => $linuxAgent]),
            ];

        foreach ($requests as $realType => $request) {
            $data = AgentLogger::log($request);

            if ($data['platform'] != $realType) {
                $this->assertTrue(false);
            }
        }

        $this->assertTrue(true);
    }


    /**
     * @param $method
     * @param $content
     * @param string $uri
     * @param array $server
     * @param array $parameters
     * @param array $cookies
     * @param array $files
     * @return \Illuminate\Http\Request
     */
    protected function createRequest($method, $content, $uri = '/test', $server = ['CONTENT_TYPE' => 'application/json'], $parameters = [], $cookies = [], $files = [])
    {
        $request = new \Illuminate\Http\Request;
        return $request->createFromBase(
            \Symfony\Component\HttpFoundation\Request::create(
                $uri,
                $method,
                $parameters,
                $cookies,
                $files,
                $server,
                $content
            )
        );
    }
}
